#!/bin/bash

cd "$(dirname "$0")"

echo "Installed the SonarQube tools..."
if [ ! -d $HOME/.sonar ]; then
    mkdir $HOME/.sonar
else
    rm -rf $HOME/.sonar/sonar* $HOME/.sonar/build*
fi

if [ -z "$1" ]; then
    export SONAR_SCANNER_VERSION=5.0.1.3006
else
    export SONAR_SCANNER_VERSION=$1
fi
    
#export SONAR_HOST=us02592nb
export SONAR_SCANNER_HOME=$HOME/.sonar/sonar-scanner-$SONAR_SCANNER_VERSION-linux

#set no proxy since the sonar host is local to our network
export NO_PROXY=us02592nb,localhost,127.0.0.1,10.96.0.0/12,192.168.59.0/24,192.168.49.0/24,192.168.39.0/24,*.vector.int,*.vectors.com
echo $NO_PROXY


# download sonar-scanner
curl -sSLo $HOME/.sonar/sonar-scanner.zip https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-$SONAR_SCANNER_VERSION-linux.zip 
unzip -q -o $HOME/.sonar/sonar-scanner.zip -d $HOME/.sonar/
export PATH=$SONAR_SCANNER_HOME/bin:$PATH
export SONAR_SCANNER_OPTS="-server"

# download build-wrapper
curl -sSLo $HOME/.sonar/build-wrapper-linux-x86.zip http://sonarcloud.io/static/cpp/build-wrapper-linux-x86.zip
unzip -q -o $HOME/.sonar/build-wrapper-linux-x86.zip -d $HOME/.sonar/
export PATH=$HOME/.sonar/build-wrapper-linux-x86:$PATH

echo "Complete."

echo "Running build-wrapper..."

if [ -d CurrentRelease/bw-output ]; then
    rm -rf CurrentRelease/bw-output
fi

source  setenv.sh

cd CurrentRelease
chmod +x make.sh
build-wrapper-linux-x86-64 --out-dir bw-output ./make.sh
cd ..

echo "Complete."
echo "Running sonar-scanner and pushing..."

sonar-scanner -Dsonar.projectKey=Post_AVT -Dsonar.cfamily.build-wrapper-output=CurrentRelease/bw-output -Dsonar.host.url=http://us02592nb:9900/ -Dsonar.token=sqp_d9c6a701aa93e33b204badf5f9b56cab6eb4e484 -Dsonar.lauguage=c -Dsonar.sources=CurrentRelease -Dsonar.exclusions=CurrentRelease/vcast-workarea/** -Dsonar.cfamily.cppunit.reportsPath=my_output_dir/xml_data/sonarqube -Dsonar.cfamily.cobertura.reportPaths=my_output_dir/xml_data/cobertura/coverage_results_PointOfSales_Manage.xml

echo "Complete."
