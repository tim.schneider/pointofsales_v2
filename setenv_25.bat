if "%WORKSPACE%" == "" set WORKSPACE=%cd%

set path=%path%;%VECTORCAST_DIR%\mingw\bin
set VCAST_DEMO_SRC_BASE=%~dp0CurrentRelease

set SQ_INSTALL_DIR=D:\vector\tools\sonarqube\sonarqube-2025.1.0.102418\bin\windows-x86-64
set PCLC_INSTALL_DIR=D:\vector\tools\pc-lint-plus\2.2\pclp
set path=%SQ_INSTALL_DIR%\jdk-17.0.8.1\bin;%SQ_INSTALL_DIR%\build-wrapper-win-x86;%SQ_INSTALL_DIR%\sonar-scanner-6.2.1.4610-windows-x64\bin;%PCLC_INSTALL_DIR%;%PATH%
