# Installation
This repository uses a git submodule so make sure when you clone this repository, clone with _--recurse-submodules_ option

# this demo is trying to expand the usefulness of the restaurant point of sales terminal
Demonstration/Testbed for the integration between VectorCAST and GitLab
       
# Installation

This repository uses a git submodule so make sure when you clone this repository, clone with _--recurse-submodules_ option

# Summary

The [VectorCAST Integration with GitLab](https://gitlab.com/tim.schneider/vectorcast_gitlab) allows the user to execute
[VectorCAST](http://vector.com/vectorcast) projects in GitLab

Results can be published in the following formats
* Coverage results to **_GitLab_** and **_SonarQube_**: **_Cobertura_** (xml_data/cobertura)
* Test Results
    * To **_GitLab_**: **_JUnit_** (xml_data/junit)
    * To **_SonarQube_**: **_CppUnit_** (xml_data/sonarqube) 
* Code Quality
    * [PC-lint Plus](https://pclintplus.com/) XML to GitLab JSON format (xml_data/pclp/gl-code-quality-report.json)

:warning: Due to the limiations of Cobertura plugin, only Statement and Branch results are reported

Two GitLab YAML CI files are provided:

* linux/windows_execute.gitlab-ci.yml - Parallel/Serial build-execute of a VectorCAST Project with options of result output format 


# Licensing Information

The MIT License

Copyright 2020 Vector Informatik, GmbH.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
