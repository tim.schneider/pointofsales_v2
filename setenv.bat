if "%WORKSPACE%" == "" set WORKSPACE=%cd%

set path=%path%;%VECTORCAST_DIR%\mingw\bin
set VCAST_DEMO_SRC_BASE=%~dp0CurrentRelease

set SQ_INSTALL_DIR=D:\vector\tools\sonarqube\sonarqube-10.2.1.78527\bin\windows-x86-64
set PCLC_INSTALL_DIR=D:\vector\tools\pc-lint-plus\2.2\pclp
set path=%SQ_INSTALL_DIR%\jdk-17.0.8.1\bin;%SQ_INSTALL_DIR%\build-wrapper-win-x86;%SQ_INSTALL_DIR%\sonar-scanner-5.0.1.3006-windows\bin;%PCLC_INSTALL_DIR%;%PATH%
