echo Making PoST.exe 


rm -rf post.exe 
rm -rf build/*.o
rm -rf ../bw-output

echo Starting Make > build.log
gcc -g -I encrypt/inc -I order_entry/inc -I database/inc -I utils/inc -o build/database.o        -c database/src/database.c          >> build.log 2>&1
gcc -g -I encrypt/inc -I order_entry/inc -I database/inc -I utils/inc -o build/encrypt.o         -c encrypt/src/encrypt.c            >> build.log 2>&1
gcc -g -I encrypt/inc -I order_entry/inc -I database/inc -I utils/inc -o build/matrix_multiply.o -c encrypt/src/matrix_multiply.c    >> build.log 2>&1
gcc -g -I encrypt/inc -I order_entry/inc -I database/inc -I utils/inc -o build/manager.o         -c order_entry/src/manager.c          >> build.log 2>&1
gcc -g -I encrypt/inc -I order_entry/inc -I database/inc -I utils/inc -o build/manager_driver.o  -c main/pos_driver.c                  >> build.log 2>&1
gcc -g -I encrypt/inc -I order_entry/inc -I database/inc -I utils/inc -o build/waiting_list.o    -c order_entry/src/waiting_list.c     >> build.log 2>&1
gcc -g -I encrypt/inc -I order_entry/inc -I database/inc -I utils/inc -o build/whitebox.o        -c utils/src/whitebox.c             >> build.log 2>&1
gcc -g -I encrypt/inc -I order_entry/inc -I database/inc -I utils/inc -o build/linked_list.o     -c utils/src/linked_list.c          >> build.log 2>&1

gcc build/*.o -o post >> build.log 2>&1
status=$?

echo Completed Make >> build.log

cat build.log 
