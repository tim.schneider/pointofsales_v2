@echo off

pushd %~dp0

if "%1"=="SQ" goto  SONARQUBE
if "%1"=="WNG" goto WARNINGSNG

set LINT_LIST=co-gcc.lnt env-xml.lnt -w1 au-misra-cpp.lnt -wlib=4 -wlib=1 post_file_list.lnt
set LINT_OUTPUT=pclp_post_results.xml
goto PCLP

:WARNINGSNG
set LINT_LIST=co-gcc.lnt env-warnings-ng.lnt -w1 au-misra-cpp.lnt -wlib=4 -wlib=1 post_file_list.lnt
set LINT_OUTPUT=pclp_post_lint_results.txt
goto PCLP

:SONARQUBE
set LINT_LIST=post_sq.lnt
set LINT_OUTPUT=pclp_post_sq_results.xml
goto PCLP

:PCLP
pclp64 %LINT_LIST%  > %LINT_OUTPUT%     

popd