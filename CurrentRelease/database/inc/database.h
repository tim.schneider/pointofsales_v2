#ifndef __DATABASE_H
#define __DATABASE_H

#include "ctypes.h"

extern data_type Get_Record(const int32_t Table);
extern void Update_Record(const int32_t Table, const data_type Data);
extern void Remove_Record(const int32_t Table);


#endif